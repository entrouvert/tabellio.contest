from five import grok
from zope import schema
from zope.interface import implements
from Products.Five.browser import BrowserView

from plone.directives import form, dexterity
from plone.dexterity.content import Item

from tabellio.contest.interfaces import MessageFactory as _

class IParticipant(form.Schema):
    name = schema.TextLine(title=_(u'Name'))
    address = schema.TextLine(title=_(u'Address'))
    zipcode = schema.TextLine(title=_(u'Zipcode'))
    locality = schema.TextLine(title=_(u'Locality'))
    phone = schema.TextLine(title=_(u'Phone'))
    email = schema.TextLine(title=_(u'Email'))

    answer1 = schema.TextLine(title=_(u'Answer 1'))
    answer2 = schema.TextLine(title=_(u'Answer 2'))

    datetime = schema.Datetime(title=_(u'Date/time'))
    ipaddress = schema.TextLine(title=_(u'IP Address'))

    removed = schema.Bool(title=_(u'Removed'), default=False)
    winner = schema.Bool(title=_(u'Winner'), default=False)


class Participant(Item):
    implements(IParticipant)

    def is_correct(self):
        correct_answer1 = self.aq_parent.question1correctanswer
        correct_answer2 = self.aq_parent.question2correctanswer
        return (correct_answer1 == self.answer1 and
                correct_answer2 == self.answer2)

    def classes(self):
        classes = []
        if self.removed:
            classes.append('removed')
        if self.is_correct:
            classes.append('correct')
        if self.winner:
            classes.append('winner')
        return ' '.join(classes)

class Remove(BrowserView):
    def __call__(self):
        self.context.removed = True
        self.context.winner = False
        self.request.response.redirect('../participants')

