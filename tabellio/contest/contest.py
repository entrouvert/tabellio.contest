import datetime
import random

from five import grok
from zope import component, schema
from Acquisition import aq_inner
from AccessControl import getSecurityManager
from Products.Five.browser import BrowserView
from Products.CMFCore.utils import getToolByName

from plone.directives import form, dexterity
from plone.dexterity.utils import createContentInContainer

from plone.registry.interfaces import IRegistry
from tabellio.config.interfaces import ITabellioSettings

from tabellio.contest.interfaces import MessageFactory as _

class IContest(form.Schema):
    title = schema.TextLine(title=_(u'Title'))

    intro = schema.Text(title=_(u'Introduction'))
    question1 = schema.TextLine(title=_(u'First question'))
    question1answers = schema.Text(title=_(u'Answers to first question'),
        description=_(u'One answer per line'))
    question1correctanswer = schema.TextLine(
                    title=_(u'Correct answer to first question'))

    question2 = schema.TextLine(title=_(u'Second question'))
    question2answers = schema.Text(title=_(u'Answers to second question'),
        description=_(u'One answer per line'))
    question2correctanswer = schema.TextLine(
                    title=_(u'Correct answer to second question'))

    active = schema.Bool(title=_(u'Active'), default=False)


class View(grok.View):
    grok.context(IContest)
    grok.require('zope2.View')

    def question1answerslist(self):
        return self.context.question1answers.split('\n')

    def question2answerslist(self):
        return self.context.question2answers.split('\n')

    def can_manage(self):
        return getSecurityManager().checkPermission('tabellio.contest.Manage',
                        aq_inner(self.context))


class Thanks(grok.View):
    grok.context(IContest)
    grok.require('zope2.View')


class Participants(grok.View):
    grok.context(IContest)
    grok.require('tabellio.contest.Manage')

    def has_winners(self):
        for object in self.context.objectValues():
            if object.winner and not object.removed:
                return True
        return False

    def winners(self):
        winners = []
        for object in self.context.objectValues():
            if object.winner and not object.removed:
                winners.append(object)
        return winners


class Participate(BrowserView):
    def __call__(self):
        plone_tool = getToolByName(self.context, 'plone_utils')
        portal_types = getToolByName(self.context, "portal_types")

        new_id = plone_tool.normalizeString(self.request.form.get('name'))
        if new_id in self.context:
            counter = 1
            while True:
                if '%s-%s' % (new_id, counter) in self.context:
                    counter += 1
                    continue
                new_id = '%s-%s' % (new_id, counter)
                break

        for attr in ('name', 'address', 'zipcode', 'locality', 'phone',
                     'email', 'answer1', 'answer2'):
            if not self.request.form.get(attr, '').strip():
                if attr == 'phone': # optional
                    continue
                self.context.plone_utils.addPortalMessage(
                        _('You must fill all fields to participate.'),
                        type='warning')
                self.request.response.redirect('./')
                return

        type_info = portal_types.getTypeInfo('tabellio.contest.participant')
        type_info._constructInstance(self.context, new_id)
        object = self.context[new_id]
        for attr in ('name', 'address', 'zipcode', 'locality', 'phone',
                     'email', 'answer1', 'answer2'):
            setattr(object, attr, self.request.form.get(attr, '').strip())

        object.datetime = datetime.datetime.now()
        object.ipaddress = self.request._client_addr

        self.sendmail(object.email)

        self.request.response.redirect('./thanks')

    def sendmail(self, mto):
        urltool = getToolByName(self.context, 'portal_url')
        portal = urltool.getPortalObject()
        settings = component.getUtility(IRegistry).forInterface(ITabellioSettings, False)

        send_to_address = portal.getProperty('email_from_address')
        envelope_from = portal.getProperty('email_from_address')
        encoding = portal.getProperty('email_charset')

        message = settings.contest_body_email
        message = message.encode(encoding)

        self.context.MailHost.send(message, mto, envelope_from,
                        subject=settings.contest_subject_email)


class PickWinner(BrowserView):
    def __call__(self):
        potential_winners = [x for x in self.context.objectValues() if
                x.is_correct() and not x.winner and not x.removed]
        if potential_winners:
            winner = random.SystemRandom().choice(potential_winners)
            winner.winner = True
            self.context.plone_utils.addPortalMessage(
                _(u'A winner has been picked.'))
        else:
            self.context.plone_utils.addPortalMessage(
                            _(u'No winner could be found.'), type='error')
        self.request.response.redirect('./participants')


class Manage(BrowserView):
    def __call__(self):
        if self.request.form.get('participants'):
            self.request.response.redirect('./participants')
        elif self.request.form.get('close'):
            self.context.active = False
            self.request.response.redirect('./')
        elif self.request.form.get('open'):
            self.context.active = True
            self.request.response.redirect('./')
        elif self.request.form.get('back'):
            self.request.response.redirect('./')

